//
//  CellFilter.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 07/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI

class Format : Identifiable, ObservableObject {
    var id = UUID()
    @Published var isSelected : Bool = false
    let title : String
    let type : FormatType
    
    init(title : String, type : FormatType) {
        self.title = title
        self.type = type
    }
    func resetFilter(){
        isSelected = false
    }
}

struct CellFilterFormat : View {
    
    @ObservedObject var format : Format
    var textCell : String
    
    @Binding var arraySelection : [Format]
    
    var body: some View {
        HStack{
            
            Text(textCell.capitalized)
            Spacer()
            Button(action: {
                self.format.isSelected.toggle()
                
                //self.disciplineLargeSelected = .physique
                print("disciplineLargeIsSelected1.isSelected:\(self.format.isSelected)")
                //print("boolImage1:\(self.boolImage1)")
                if self.format.isSelected {
                    self.arraySelection.append(self.format)
                }
                print(self.arraySelection)
                for index in 0..<self.arraySelection.count{
                    print(self.arraySelection[index].isSelected)
                }
                
            }, label: {
                Image(self.format.isSelected ? "icons8-checked_checkbox" : "icons8-unchecked_checkbox-1").resizable().checkMultiChoiceButtonStyle()
            })
        }
    }
}

//struct CellFilter_Previews: PreviewProvider {
//    static var previews: some View {
//        CellFilterFormat()
//    }
//}
