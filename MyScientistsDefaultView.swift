//
//  MyScientistsDefaultView.swift
//  MySciences
//
//  Created by Alice Grele on 08/07/2020.
//  Copyright © 2020 Alice Grele. All rights reserved.
//


import SwiftUI

struct MyScientistsDetailView: View {
  //  @ObservedObject var mySource: Sources //= TestClass()
    @EnvironmentObject var favorites : Favorites
    var source : Source
    var body: some View {
        VStack {
            Image(source.imageSpotlight.isEmpty ? source.imageDetail : source.imageSpotlight)
                .resizable()
                .scaledToFit()
                .frame(width: self.largeurRectangle(), height: self.hauteurRectangle())
                .cornerRadius(20)
                .padding(.top)
            Text(source.detailText)
                .font(.subheadline)
                .padding()
            Text(String(source.lien ?? ""))
                .font(.footnote)
                .underline()
            Spacer()
                .navigationBarTitle(Text(source.nom), displayMode: .inline)
            .navigationBarItems(trailing: Button(action: {
                if self.favorites.containsScientist(self.source) {
                    self.favorites.removeScientist(self.source)
                } else {
                    self.favorites.addScientist(self.source)
                }
            }, label: {
                HStack() {
                    Image(systemName: favorites.containsScientist(self.source) ? "bookmark.fill" : "bookmark")
                    .resizable()
                    .foregroundColor(.black)
                    .frame(width: 20, height: 30)
                }
            }))
            
        }
    }    // 5 is the number of spacings in the HStack row
    // 12 is the spacing in the HStack between buttons
    func hauteurRectangle() -> CGFloat {
        return(UIScreen.main.bounds.height - 1 * 27 ) / 4
    }
    func largeurRectangle() -> CGFloat {
        return(UIScreen.main.bounds.width - 2 * 13 ) / 1
    }
}

struct MyScientistView: View {
  //  @ObservedObject var sourcesList: Sources
    @Environment(\.presentationMode) var presentationMode
    @State var activated: Bool = false
    @State var isPresenting : Bool = false
    var resultView : some View {
    
            VStack {
                List {
                   // Section(header:
                   //     Text("SANTE").font(.headline).fontWeight(.bold).foregroundColor(.gray)
                  //  ) {
                        ForEach (allScientists) { source in
                            DefaultCellView(source: source)
                            
                    }
             //   }
                }
                .background(Color.blue)
                .onAppear() {
                UITableView.appearance().separatorStyle = .none
                // enleve le background color du header de la liste
                UITableViewHeaderFooterView.appearance().tintColor = .white
                
            }
                
                // enleve les separateurs de la liste
                
                    
            }.navigationBarTitle("Résultats", displayMode: .inline)
        
    }
    
    var defaultView : some View {

                VStack (alignment: .leading, spacing: 5) {
                    NavigationLink(destination: MyScientistsDetailView(source : scientistSpotlight)){
                        BigSpotlightView(source: scientistSpotlight, extra: false)
                          //  .padding(.vertical, 2.0)
                          //  .padding(.horizontal, 5.0)
                        .padding()
                    }
                    .buttonStyle(PlainButtonStyle())
                    List {
                        ForEach (listeScientistsDefaut) { scientist in
                            NavigationLink(destination: MyScientistsDetailView(source : scientist)) {SmallSpotlightView(source: scientist, extra: false)}
                        }
                    }
                }.navigationBarTitle("MyScientists")
            .onAppear() {
            UITableView.appearance().separatorStyle = .none
            UITableViewHeaderFooterView.appearance().tintColor = .white
                    
                //   .offset(x: 0, y: -15)
                //  .padding([.leading, .bottom, .trailing])
            }
                
        
    }
    
    var body: some View {
        NavigationView {
        VStack {
            if activated == true{
                resultView
            }
            else {
                defaultView
            }
        }/*.navigationBarItems(trailing: Button(action: {self.isPresenting.toggle()}, label: {
            HStack(spacing: 5) {
                Text("Filtrer").foregroundColor(.gray)
                Image(systemName: "slider.horizontal.3")
                    .foregroundColor(.black)

            }
        }).sheet(isPresented: $isPresenting, content: {
            SourceFilterModal(presenting: self.$isPresenting, activated: self.$activated, agreedToTerms: false)
        })) */
    }
    }
}

struct MyScientistsDefaultView_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
    }
}
