//
//  CellFilterLangues.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 07/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI

struct CellFilterLangues: View {
    let langue : Langues
    @ObservedObject var selectedLanguages : SelectedLanguages
    
    var body: some View {
        HStack{
            
            Text(langue.title)
            Spacer()
            Button(action: {
                self.selectedLanguages.dicLangues[self.langue.key]?.toggle()
            }, label: {
                Image(self.selectedLanguages.dicLangues[langue.key] == true ? "icons8-checked_checkbox" : "icons8-unchecked_checkbox-1").resizable().checkMultiChoiceButtonStyle()
            })
        }
    }
}
//
//struct CellFilterLangues_Previews: PreviewProvider {
//    static var previews: some View {
//        CellFilterLangues()
//    }
//}
