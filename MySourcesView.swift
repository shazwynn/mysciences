//
//  MySourcesDefaultView.swift
//  MySciences
//
//  Created by Alice Grele on 08/07/2020.
//  Copyright © 2020 Alice Grele. All rights reserved.
//

import SwiftUI

struct AppView: View {
    @ObservedObject var favorites = Favorites()
    @ObservedObject var results = Results()
    var body: some View{
        TabView {
            MySourcesDefaultView()
                .tabItem {
                    Image(systemName: "book")
                    Text("MySources")}
            MyScientistView()
                .tabItem {
                    Image(systemName: "person")
                    Text("MyScientists")}
            
            MyFavorisView()
                .tabItem {
                    Image(systemName: "bookmark")
                    Text("MyFavoris")}
            
        }
        .environmentObject(favorites)
        .environmentObject(results)
        // .padding(.bottom, 5.0)
    }
}

class SelectedLanguages : ObservableObject {
    @Published var dicLangues : [String: Bool]
    init() {
        dicLangues = [:]
        for i in 0..<allLangues.count {
            dicLangues[allLangues[i].key] = false
        }
    }
    func resetDic() {
        for (key, _) in dicLangues {
            dicLangues[key] = false
        }
    }
}

struct  MySourcesDefaultView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var activated: Bool = false
    @State var isPresenting : Bool = false
    @EnvironmentObject var results: Results
    @ObservedObject var selectedChoicesToggledSupport = SelectedChoicesToggledFormat()
    @ObservedObject var selectedChoicesToggledDSpe = SelectedChoicesToggledDSpe()
    @State var selectedLanguages = SelectedLanguages()
    @State var indexDiscipline : DisciplineType = .testAllDisciplines
    
    var body: some View {
        NavigationView {
         //   VStack {
                List {
                    if activated == true{
                        ForEach (allSources) { source in
                            if self.results.containsSource(source) {
                                NavigationLink(destination: MySourcesDetailView(source: source)) {
                                    DefaultCellView(source: source)}
                            } }
                    }
                    else {
                        ForEach (listeSourcesDefaut.indices) // only 4
                        { index in
                            NavigationLink(destination: MySourcesDetailView(source: listeSourcesDefaut[index])) {
                                if index == 0 {
                                    BigSpotlightView(source: listeSourcesDefaut[index], extra: true).padding(.bottom)}
                                else {
                                    SmallSpotlightView(source: listeSourcesDefaut[index], extra: true)
                                }
                            }
                        }
                    }
                }.navigationBarTitle(activated ? "Résultats" : "MySources", displayMode: activated ? .inline : .automatic)
                    .onAppear() {
                        UITableView.appearance().separatorStyle = .none
                        // enleve le background color du header de la liste
                        UITableViewHeaderFooterView.appearance().tintColor = .white
                }
         //   }
            .navigationBarItems(trailing: Button(action: {self.isPresenting.toggle()}, label: {
                HStack(spacing: 5) {
                    Text("Filtrer").foregroundColor(.gray)
                    Image(systemName: "slider.horizontal.3")
                        .foregroundColor(.black)
                    
                }
            }).sheet(isPresented: $isPresenting, content: {
                SourceFilterModal(presenting: self.$isPresenting, activated: self.$activated, selectedChoicesToggledSupport: self.selectedChoicesToggledSupport, selectedChoicesToggledDSpe: self.selectedChoicesToggledDSpe, indexDiscipline: self.$indexDiscipline,agreedToTerms: false, selectedLanguages: self.selectedLanguages)
                    .environmentObject(self.results)
            }))
        }
    }
}

struct DefaultCellView: View {
    var source : Source
    var body: some View {
        HStack{
            Image(source.image)
            .resizable()
                .frame(width: self.coteCarre(), height: self.coteCarre())
                .cornerRadius(10)
            VStack (alignment: .leading){
                Text(source.sourceText(source: source))
                    .font(.headline)
                Text(source.descriptionText)
                    .font(.subheadline)
            }
        }
    }
    func coteCarre() -> CGFloat {
        return(UIScreen.main.bounds.height - 7 * 8 ) / 8
    }
}

struct SmallSpotlightView: View {
    //   let imageCarree : String
    let source: Source
    let extra: Bool
    
    var body: some View {
        HStack {
            Image(source.image)
                .resizable()
                .frame(width: self.coteCarre(), height: self.coteCarre())
                .cornerRadius(10)
            VStack (alignment : .leading){
                Text(extra ? source.sourceText(source: source) : source.nom)
                    .font(.headline)
                Text(source.descriptionText)
                    .font (.subheadline)
            }
        }
    }
    func coteCarre() -> CGFloat {
        return(UIScreen.main.bounds.height - 7 * 8 ) / 8
    }
}

struct BigSpotlightView: View {
    let source: Source
    let extra: Bool
    var body: some View {
        VStack(alignment: .center) {
            //   Image(systemname: "rectanglegris")
            Image(source.imageSpotlight.isEmpty ? source.imageDetail : source.imageSpotlight)
                .resizable()
                .scaledToFit()
                .cornerRadius(20)
                .frame(width: self.largeurRectangle(), height: self.hauteurRectangle())
            
            Text(extra ? source.sourceText(source: source) : source.nom)
                .font(.headline)
                .fontWeight(.bold)
            Text(source.descriptionText)
                .font (.subheadline)
                .multilineTextAlignment(.center)
        }
    }
    // 5 is the number of spacings in the HStack row
    // 12 is the spacing in the HStack between buttons
    func hauteurRectangle() -> CGFloat {
        return(UIScreen.main.bounds.height - 4 * 27 ) / 4
    }
    func largeurRectangle() -> CGFloat {
        return(UIScreen.main.bounds.width - 2 * 13 ) / 1
    }
}

struct AppView_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
    }
}
