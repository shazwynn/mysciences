//
//  FormatView.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 07/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI

var arVr = Format(title: "réalite virtuelle et augmentée", type: .arVr)
var articlesVulgarisation = Format(title: "vulgarisation", type: .articleVulgarisation)
var audio = Format(title: "audio", type: .audio)
var cours = Format(title: "cours", type: .cours)
var evenement = Format(title: "évènement", type: .evenement)
var jeu = Format(title: "jeu", type: .jeu)
var ouvrage = Format(title: "ouvrage", type: .ouvrage)
var publicationScientifique = Format(title: "publication", type: .publicationScientifique)
var texteContenuInstructif = Format(title: "article éducatif", type: .texteContenuInstructif)
var video = Format(title: "video", type: .video)

//let allFormat = [arVr, articlesVulgarisation, audio, cours, evenement, jeu, ouvrage, publicationScientifique, texteContenuInstructif, video]
// ordre plus logique, vérifier ce que ca donne sur le picker
let allFormat = [cours, texteContenuInstructif, ouvrage, articlesVulgarisation, publicationScientifique, audio, video, jeu, evenement, arVr ]

struct FormatView: View {

    @Binding var formatsSelected : [Format]
    var body: some View {
        ForEach(allFormat) { formats in
            CellFilterFormat(format: formats, textCell: formats.title, arraySelection: self.$formatsSelected)
        }
    }
}

//struct FormatView_Previews: PreviewProvider {
//    static var previews: some View {
//        FormatView()
//    }
//}
