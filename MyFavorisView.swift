//
//  MyFavorisView.swift
//  MySciences
//
//  Created by Alice Grele on 08/07/2020.
//  Copyright © 2020 Alice Grele. All rights reserved.
//

import SwiftUI

struct bookmarkToggleStyle: ToggleStyle {
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
            Image(systemName: configuration.isOn ? "bookmark.fill" : "bookmark")
                .onTapGesture {
                    configuration.isOn.toggle()
            }
        }
    }
}

extension Image {
    // group modifiers for images in MyFavoris
    func favorisImageModifier() -> some View {
        self
            .resizable()
            .frame(width: self.favorisCoteCarre(), height: self.favorisCoteCarre())
            .cornerRadius(10)
    }
    func favorisCoteCarre() -> CGFloat {
        return(UIScreen.main.bounds.height - 7 * 8 ) / 8
    }
}

struct FavorisCellView: View {
    @EnvironmentObject var favorites: Favorites
    @State var source : Source
    @ViewBuilder
    var body: some View {
        HStack{
            Image(source.image)
                .favorisImageModifier()
            VStack (alignment: .leading){
                HStack {
                    Text(source.sourceText(source: source))
                        .font(.subheadline)
                        .bold()
                    // activate following spacer to align with right side of screen
                    Spacer()
                    Button(action: {
                        if self.favorites.containsSource(self.source){
                            self.favorites.removeSource(self.source)
                        }
                        else if self.favorites.containsScientist(self.source) {
                            self.favorites.removeScientist(self.source)
                        }
                    }) {
                        Image(systemName: favorites.containsSource(source) || favorites.containsScientist(source) ? "bookmark.fill" : "bookmark")
                        .resizable()
                        .foregroundColor(.black)
                        .frame(width: 20, height: 30)
                    }
                    Spacer()
                        .frame(width: 15.0, height: 15.0)
                }
                Text(source.descriptionText)
                    .font(.subheadline)
            }
        }
    }
}
/*
func onlyFavorites (sL : [Source], f : Favorites) -> [Source]{
    var favoriteList : [Source] = []
    sL.forEach {source in
        if f.containsSource(source) {
            favoriteList.append(source)
        }
    }
    return favoriteList
}
*/
struct MyFavorisView: View {
    @EnvironmentObject var favorites: Favorites
   // @EnvironmentObject var sourcesList: Sources
    @State var segmentedControlCategory = 0
    @ViewBuilder
    var body: some View {
        NavigationView {
            VStack {
                MyFavorisPickerView(segmentedControlCategory: $segmentedControlCategory)
                if segmentedControlCategory == 1 {
                    supportFavorisView(favoriteList: getFavoritedSources(liste: allSources))}
                else if segmentedControlCategory == 2 {
                    scientistFavorisView(favoriteList: getFavoritedScientists(liste: allScientists))
                    Spacer()
                }
                else {disciplineFavorisView(favoriteList: getFavoritedSources(liste: allSources))}
            }
                // enleve les separateurs de la liste
                .onAppear() {
                    UITableView.appearance().separatorStyle = .none
            }.navigationBarTitle("MyFavoris")
        }
    }
    func getFavoritedSources (liste : [Source]) -> [Source] {
        var favoritesList : [Source] = []
        for source in liste {
            if self.favorites.containsSource(source) {
                favoritesList.append(source)
            }
        }
        return favoritesList
    }
    func getFavoritedScientists (liste : [Source]) -> [Source] {
        var favoritesList : [Source] = []
        for scientist in liste {
            if self.favorites.containsScientist(scientist) {
                favoritesList.append(scientist)
            }
        }
        return favoritesList
    }
}

struct oneCategoryView: View {
    var filteredList : [Source]
    @EnvironmentObject var favorites: Favorites
    var title : String
    var count = 0
    @ViewBuilder var body: some View {
        if filteredList.count != 0 {
            Section(header: doINeedHeader(list: filteredList) ?
                Text(title.uppercased()).font(.headline) : Text("")
            ) {
                ForEach (filteredList) { source in
                    // since the nav link is on the entire view, it makes the bookmark button unsuable (it's part of the FavorisCellView)
                    if self.favorites.containsSource(source){
                        NavigationLink(destination: MySourcesDetailView(source : source)) {
                        FavorisCellView(source: source)
                        }
                    }
                    else if self.favorites.containsScientist(source) {
                        NavigationLink(destination: MyScientistsDetailView(source : source)) {
                        FavorisCellView(source: source)
                        }
                    }
                }
            }
        }
        else {
            EmptyView()
        }
    }
    func doINeedHeader(list : [Source]) -> Bool {
        for source in list {
            if self.favorites.containsSource(source) || self.favorites.containsScientist(source){
                return true
            }
        }
        return false
    }
}
/*
func doINeedHeader (title : String, sourcesList : [Source]) -> Bool
{
    for source in sourcesList {
        if self.favorites.contains(source) {
            
        }
    }
    return true
}
*/
struct scientistFavorisView : View {
    @State var favoriteList : [Source]
    var body: some View {
        List {
            ForEach(allDiscipline.indices) { x in
                oneCategoryView(filteredList: self.getFilteredSources(liste: self.favoriteList, discipline: allDiscipline[x].type),title: allDiscipline[x].title)
            }
        }
    }
    func getFilteredSources (liste : [Source], discipline : DisciplineType) -> [Source] {
        let filteredList = liste.filter({$0.disciplinesLarges.type == discipline
            }
        )
        return filteredList
    }
}

struct supportFavorisView : View {
    @State var favoriteList : [Source]
    var body: some View {
        List {
            ForEach(allFormat.indices) { f in
                oneCategoryView(filteredList: self.getFilteredSources(liste: self.favoriteList, format: allFormat[f].title),title: allFormat[f].title)
            }
        }
    }
    func getFilteredSources (liste : [Source], format : String) -> [Source] {
        let filteredList = liste.filter({$0.format == format})
        return filteredList
    }
}

struct disciplineFavorisView : View {
    // @ObservedObject var mySources: Sources
    @State var favoriteList : [Source]
    var body: some View {
        List {
            ForEach(allDiscipline.indices) { x in
                oneCategoryView(filteredList: self.getFilteredSources(liste: self.favoriteList, discipline: allDiscipline[x].type),title: allDiscipline[x].title)
            }
        }
    }
    func getFilteredSources (liste : [Source], discipline : DisciplineType) -> [Source] {
        let filteredList = liste.filter({$0.disciplinesLarges.type == discipline
            }
        )
        return filteredList
    }
}

//self.favorites.contains(source)
struct MyFavorisPickerView: View {
    @Binding var segmentedControlCategory : Int
    var body: some View {
        Picker(selection: $segmentedControlCategory, label: Text("")) {
            Text("Discipline").tag(0)
            Text("Type de support").tag(1)
            Text("Scientifiques").tag(2)
        }
        .pickerStyle(SegmentedPickerStyle())
        .padding()
    }
}

struct MyFavorisView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        AppView()
    }
}

