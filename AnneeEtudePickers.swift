//
//  AnneeEtudePickers.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 07/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI

struct AnneeEtudePickers: View {
    @Binding var index : AnneeEtude

    var body: some View {
        Picker("Votre niveau d'études", selection: $index) {

//                Section(header: Text("Primaire")) {
                    ForEach(allAnneePrimaire) {
                        Text($0.title).tag($0.type)
                    }
            
//                }
//                Section(header: Text("Collège")) {
                    ForEach(allAnneeCollege) {
                        Text($0.title).tag($0.type)
                        
                    }
//                }
//                Section(header: Text("Lycée")) {
                    ForEach(allAnneeLycee) {
                        Text($0.title).tag($0.type)
                    }
//                }
//                Section(header: Text("Études supérieure")) {
                    ForEach(allAnneeSup) {
                        Text($0.title).tag($0.type)
                    }
//                }
        }
        
    }
}

struct AnneeEtudePickers_Previews: PreviewProvider {
    static var previews: some View {
        AnneeEtudePickers(index: .constant(.nonRenseigne))
    }
}
