//
//  ExtensionViewStyle.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 02/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI


//Extensions pour la Navigation Bar Filter

//bouton gauche (leading)
extension View {
    func navBarItemLeadingStyle() -> some View {
        self.font(.system(size: 20)).foregroundColor(.black).frame(width :28, height: 28)
    }
}

//bouton droite (trailing)
extension View {
    func navBarItemTrailingStyle() -> some View {
        self.font(.body).foregroundColor(.gray)
    }
}

//extension bouton check multichoix
extension View {
    func checkMultiChoiceButtonStyle() -> some View {
        self.scaledToFit().frame(width : 25, height : 30)
    }
}


///////////////////////////////////

//Extensions pour la Nav bar MySource

//Bouton filtrer nav bar MySource
extension View {
    func navBarTrailingButtonStyle() -> some View {
        self.font(.system(size: 20)).padding(6).foregroundColor(.black)
    }
}
