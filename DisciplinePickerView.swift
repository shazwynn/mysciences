//
//  DisciplinePickerView.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 07/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI

struct DisciplinePickerView: View {
    @Binding var index : DisciplineType
    
    var body: some View {
        Picker("Disciplines", selection: $index) {
            ForEach(allDiscipline) { discipline in
                Text(discipline.title).tag(discipline.type)
            }
            
        }.onReceive([self.index].publisher.first()) { (value) in
            for i in 0..<allDisciplinesSpe.count {
                
                for t in 0..<allDisciplinesSpe[i].count {
                    allDisciplinesSpe[i][t].resetFilter()
                }
            }
        }
    }
}

struct DisciplinePickerView_Previews: PreviewProvider {
    static var previews: some View {
        DisciplinePickerView(index: .constant(.generaliste))
    }
}
