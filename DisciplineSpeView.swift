//
//  DisciplineSpeView.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 08/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI

var c1 = DisciplineSpe(title: "Analytique", type: .chimie)
var c2 = DisciplineSpe(title: "Biochimie", type: .chimie)
var c3 = DisciplineSpe(title: "Inorganique", type: .chimie)
var c4 = DisciplineSpe(title: "Histoire", type: .chimie)
var c5 = DisciplineSpe(title: "Organique", type: .chimie)

var p1 = DisciplineSpe(title: "Astrophysique", type: .physique)
var p2 = DisciplineSpe(title: "Histoire", type: .physique)
var p3 = DisciplineSpe(title: "Mécanique", type: .physique)
var p4 = DisciplineSpe(title: "Particules", type: .physique)
var p5 = DisciplineSpe(title: "Thermodynamique", type: .physique)

var i1 = DisciplineSpe(title: "Algorithmie", type: .informatique)
var i2 = DisciplineSpe(title: "Data science", type: .informatique)
var i3 = DisciplineSpe(title: "Histoire", type: .informatique)
var i4 = DisciplineSpe(title: "Intelligence artificielle", type: .informatique)
var i5 = DisciplineSpe(title: "Sécurité", type: .informatique)

var anatomie = DisciplineSpe(title: "Anatomie", type: .medecine)
var m2 = DisciplineSpe(title: "Epidémiologie", type: .medecine)
var m3 = DisciplineSpe(title: "Histoire", type: .medecine)
var m4 = DisciplineSpe(title: "Neuroscience", type: .medecine)
var m5 = DisciplineSpe(title: "Nutrition", type: .medecine)

var zoologie = DisciplineSpe(title: "Zoologie", type: .scienceVieTerre)
var svt2 = DisciplineSpe(title: "Bioinformatique", type: .scienceVieTerre)
var svt3 = DisciplineSpe(title: "Ecologie", type: .scienceVieTerre)
var svt4 = DisciplineSpe(title: "Génétique", type: .scienceVieTerre)
var svt5 = DisciplineSpe(title: "Histoire", type: .scienceVieTerre)

let allChimie = [c1, c2, c3, c4, c5]
let allPhysique = [p1, p2, p3, p4, p5]
let allInformatique = [i1,i2,i3,i4,i5]
let allMedecine = [anatomie,m2,m3,m4,m5]
let allSVT = [svt2,svt3,svt4,svt5, zoologie]

let allDisciplinesSpe = [allChimie, allPhysique, allInformatique, allMedecine, allSVT]

struct DisciplineSpeView: View {
    var index : DisciplineType
    
    @Binding var disciplineSpeSelected : [DisciplineSpe]
    
    var body: some View {
        ForEach(index.values) {
            CellFilterDisciplineSpe(disciplineSpe: $0, textCell: $0.title, arraySelection: self.$disciplineSpeSelected)
        }
    }
}

//struct DisciplineSpeView_Previews: PreviewProvider {
//    static var previews: some View {
//        DisciplineSpeView()
//    }
//}
