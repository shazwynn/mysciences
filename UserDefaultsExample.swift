//
//  UserDefaultsExample.swift
//  MySciences
//
//  Created by Agnes Grele on 29/07/2020.
//  Copyright © 2020 Alice Grele. All rights reserved.
//

import SwiftUI

/*
struct Task {
    var id = UUID()
    var title: String
}

struct UserDefaultsExample: View {
    @State var tasks : [Task] = [Task(title: "We Are Young"), Task(title: "Memories")]
    var body: some View {
        Text("hi")
    }
}

class FavoritesDef: ObservableObject {
    private var tasks: Set<String>
    let defaults = UserDefaults.standard
    init() {
        let decoder = JSONDecoder()
        if let data = defaults.value(forKey: "Favorites") as? Data {
            let taskData = try? decoder.decode(Set<String>.self, from: data)
            self.tasks = taskData ?? []
        } else {
            self.tasks = []
        }
    }
    
    func getTaskIds() -> Set<String> {
        return self.tasks
    }
    func isEmpty() -> Bool {
        tasks.count < 1
    }
    func contains(_ task: Task) -> Bool {
        tasks.contains(task.id)
    }
    func add(_ task: Task) {
        objectWillChange.send()
        tasks.insert(task.id)
        save()
    }
    func remove(_ task: Task) {
        objectWillChange.send()
        tasks.remove(task.id)
        save()
    }
    func save() {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(FavoritesDef) {
            defaults.set(encoded, forKey: "FavoritesDef")
        }
    }
 }


struct UserDefaultsExample_Previews: PreviewProvider {
    static var previews: some View {
        UserDefaultsExample()
    }
}
*/

// implemented in AppView()
// implement in MyFavorisView()
//struct Resort {
//    var id = UUID()
//    var name : String
//}

class Results : ObservableObject {
    private var sources: Set<String>
    private var scientists: Set<String>
    
    init() {
        self.sources = []
        self.scientists = []
    }
    func containsSource(_ source: Source) -> Bool {
        sources.contains(source.id.uuidString)
    }
    func addSource(_ source: Source) {
        objectWillChange.send()
        sources.insert(source.id.uuidString)
       // save()
    }
    func removeSource(_ source: Source) {
        objectWillChange.send()
        sources.remove(source.id.uuidString)
       // save()
    }
    // untested
    func resetSources() {
        objectWillChange.send()
        sources.removeAll()
    }
    func countSources() -> Int {
        return sources.count
    }
    func containsScientist(_ scientist: Source) -> Bool {
        scientists.contains(scientist.id.uuidString)
    }
    func addScientist(_ scientist: Source) {
        objectWillChange.send()
        scientists.insert(scientist.id.uuidString)
       // save()
    }
    func removeScientist(_ scientist: Source) {
        objectWillChange.send()
        scientists.remove(scientist.id.uuidString)
       // save()
    }
    // untested
    func resetScientists() {
        objectWillChange.send()
        scientists.removeAll()
    }
    // untested
    func resetAll() {
        resetSources()
        resetScientists()
    }
}

class Favorites: ObservableObject {
    private var sources: Set<String>
    private var scientists: Set<String>
    /* look up userdefaults implementation 1.https://medium.com/macoclock/how-to-create-a-like-button-and-save-favorite-structs-to-userdefault-in-swiftui-67ba215c358d
    2.https://www.hackingwithswift.com/books/ios-swiftui/letting-the-user-mark-favorites
     3.https://www.simpleswiftguide.com/how-to-use-userdefaults-in-swiftui/
     4.https://fluffy.es/persist-data/
 */
    
  //  private let saveKey = "Favorites"
    
    init() {
        self.sources = []
        self.scientists = []
    }
    func containsSource(_ source: Source) -> Bool {
        sources.contains(source.id.uuidString)
    }
    func addSource(_ source: Source) {
        objectWillChange.send()
        sources.insert(source.id.uuidString)
       // save()
    }
    func removeSource(_ source: Source) {
        objectWillChange.send()
        sources.remove(source.id.uuidString)
       // save()
    }
    func containsScientist(_ scientist: Source) -> Bool {
        scientists.contains(scientist.id.uuidString)
    }
    func addScientist(_ scientist: Source) {
        objectWillChange.send()
        scientists.insert(scientist.id.uuidString)
       // save()
    }
    func removeScientist(_ scientist: Source) {
        objectWillChange.send()
        scientists.remove(scientist.id.uuidString)
       // save()
    }
    
    /*
    func save() {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(Favorites) {
            defaults.set(encoded, forKey: "Favorites")
        }
    }
 */
}


struct UserDefaultsExample_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
    }
}
