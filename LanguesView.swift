//
//  LanguesView.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 07/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI

class Langues : Identifiable {
    var id = UUID()
    let title : String
    let key : String
    
    init(title : String, key : String) {
        self.title = title
        self.key = key
    }
}

//Instances
var anglais = Langues(title: "Anglais", key: "EN")
var francais = Langues(title: "Français", key: "FR")

let allLangues = [anglais, francais]

struct LanguesView: View {
    @ObservedObject var selectedLanguages : SelectedLanguages
    
    var body: some View {
        ForEach(allLangues){ langue in
            CellFilterLangues(langue : langue, selectedLanguages: self.selectedLanguages)
        }
    }
}

//struct LanguesView_Previews: PreviewProvider {
//    static var previews: some View {
//        LanguesView()
//    }
//}
