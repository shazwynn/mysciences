//
//  CellFilterDisciplineSpe.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 07/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI


class DisciplineSpe : Identifiable, ObservableObject {
    var id = UUID()
    
    let title : String
    let type : DisciplineType
    @Published var isSelected = false
    
    init(title : String, type : DisciplineType) {
        self.title = title
        self.type = type
        
    }
        func resetFilter() {
            isSelected = false
        }
}


struct CellFilterDisciplineSpe : View {
    
    @ObservedObject var disciplineSpe : DisciplineSpe
    var textCell : String
    
    @Binding var arraySelection : [DisciplineSpe]
    
    var body: some View {
        HStack{
            
            Text(textCell)
            Spacer()
            Button(action: {
                self.disciplineSpe.isSelected.toggle()
                
                //self.disciplineLargeSelected = .physique
                print("disciplineLargeIsSelected1.isSelected:\(self.disciplineSpe.isSelected)")
                //print("boolImage1:\(self.boolImage1)")
                if self.disciplineSpe.isSelected {
                    self.arraySelection.append(self.disciplineSpe)
                }
                print(self.arraySelection)
                for index in 0..<self.arraySelection.count{
                    print(self.arraySelection[index].isSelected)
                }
                
            }, label: {
                Image(self.disciplineSpe.isSelected ? "icons8-checked_checkbox" : "icons8-unchecked_checkbox-1").resizable().checkMultiChoiceButtonStyle()
            })
        }
    }
}

//struct CellFilterDisciplineSpe_Previews: PreviewProvider {
//    static var previews: some View {
//        CellFilterDisciplineSpe().environmentObject(DisciplineSpe())
//    }
//}
