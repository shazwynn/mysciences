//
//  CadreRecherchePicker.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 08/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI

struct CadreRecherchePicker: View {
   @Binding var index : CadreRechercheType
    
    var body: some View {
        Picker("Cadre de votre recherche", selection: $index) {
            ForEach(allCadres) { cadres in
                Text(cadres.title).tag(cadres.type)
            }
        }
    }
}

//struct CadreRecherchePicker_Previews: PreviewProvider {
//    static var previews: some View {
//        CadreRecherchePicker()
//    }
//}
