//
//  SwiftUIView.swift
//  MySciences
//
//  Created by Agnes Grele on 08/08/2020.
//  Copyright © 2020 Alice Grele. All rights reserved.
//

import SwiftUI

struct SwiftUIView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIView()
    }
}
