//
//  DomaineEtudePicker.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 08/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI

struct DomaineEtudePicker: View {
    
    @Binding var index : DomaineEtudeType
    
    var body: some View {
        Picker("Dans le domaine", selection: $index) {
            ForEach(allDomaineEtude) { domaine in
                Text(domaine.title).tag(domaine.type)
            }
        }
    }
}

struct DomaineEtudePicker_Previews: PreviewProvider {
    static var previews: some View {
        DomaineEtudePicker(index: .constant(.nonRenseigne))
    }
}
