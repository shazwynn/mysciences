//
//  ExtractionsViewModal.swift
//  MySciencesApp
//
//  Created by Stephano Robinson on 02/07/2020.
//  Copyright © 2020 Stephano Robinson. All rights reserved.
//

import SwiftUI

/////////////////////////

//Class selection contenant array (pour multichoice)


class SelectedChoicesToggledFormat : ObservableObject {
    @Published var selectedChoicesToggled : [Format] = []
    func resetToFalse() {
        for i in 0..<selectedChoicesToggled.count {
            selectedChoicesToggled[i].isSelected = false
        }
    }
}

class SelectedChoicesToggledDSpe : ObservableObject {
    @Published var selectedChoicesToggled : [DisciplineSpe] = []
    func resetToFalse() {
        for i in 0..<selectedChoicesToggled.count {
            selectedChoicesToggled[i].isSelected = false
        }
    }
}

////////////////

//extraction view Filter modal

struct SourceFilterModal : View {
    //pour affichage modal
    @Binding var presenting : Bool
    @Binding var activated : Bool
    @Environment(\.presentationMode) private var presentationMode
    // pour fabriquer une liste de résultats
    @EnvironmentObject var results: Results
    //@ObservedObject var sourcesList: Sources
    
    //    @ObservedObject var selectedFormat = SelectionOnOff()
    
    //Array pour objet multi choix
    @ObservedObject var selectedChoicesToggledSupport : SelectedChoicesToggledFormat
    @ObservedObject var selectedChoicesToggledDSpe : SelectedChoicesToggledDSpe
    
    
    
    //Ce que l'on recup
    @State var indexDomaineEtude : DomaineEtudeType = .nonRenseigne
    @State var indexAnneeEtude : AnneeEtude = .nonRenseigne
    @Binding var indexDiscipline : DisciplineType
    @State var indexCadreRecherche : CadreRechercheType = .nonRenseigne
    
    //bool pour disable Domaine etude
    @State var agreedToTerms : Bool
    @ObservedObject var selectedLanguages : SelectedLanguages
    //fonction agreeToTerms = true or false
    func disabling(annee : AnneeEtude) {
        return agreedToTerms = annee.disableling
    }
    
    //func reset all filters in modal
    func resetButton() {
        /* reset support & format selection */
        selectedChoicesToggledSupport.resetToFalse()
        print("selectedChoicesToggledSupport :\(selectedChoicesToggledSupport.selectedChoicesToggled)")
        selectedChoicesToggledSupport.selectedChoicesToggled.removeAll()
        
        for i in 0..<allFormat.count {
            allFormat[i].resetFilter()
        }
        /* reset languages selection */
        selectedLanguages.resetDic()
        print(selectedLanguages.dicLangues)
        
        /* reset discipline spe selection */
        selectedChoicesToggledDSpe.resetToFalse()
        print("selectedChoicesToggledDSpe :\(selectedChoicesToggledDSpe.selectedChoicesToggled)")
        selectedChoicesToggledDSpe.selectedChoicesToggled.removeAll()
        
        for i in 0..<allDisciplinesSpe.count {
            
            for t in 0..<allDisciplinesSpe[i].count {
                allDisciplinesSpe[i][t].resetFilter()
            }
        }
        /* reset other selections */
        indexDiscipline = .testAllDisciplines
        indexAnneeEtude = .nonRenseigne
        indexDomaineEtude = .nonRenseigne
        indexCadreRecherche = .nonRenseigne
        agreedToTerms = false
        
        // reset de la liste des resultats
        results.resetSources()
    }
    
    var body : some View {
        NavigationView {
            ZStack {
                Color(red: 0.949, green: 0.949, blue: 0.968).edgesIgnoringSafeArea(.bottom)
                VStack {
                    Form {
                        Section(header: Text("Discipline").bold()) {
                            DisciplinePickerView(index : $indexDiscipline)
                            
                            DisciplineSpeView(index: indexDiscipline, disciplineSpeSelected: $selectedChoicesToggledDSpe.selectedChoicesToggled)
                            
                        }
                        Section(header: Text("Pour un niveau et domaine d'étude").bold()) {
                            AnneeEtudePickers(index: $indexAnneeEtude).onAppear(){
                                self.disabling(annee: self.indexAnneeEtude)
                            }
                            //domaine étude
                            DomaineEtudePicker(index: $indexDomaineEtude).disabled(!agreedToTerms)
                        }
                        Section {
                            CadreRecherchePicker(index: $indexCadreRecherche)
                        }
                        Section(header: Text("Le format de préférence").bold()){
                            FormatView(formatsSelected: $selectedChoicesToggledSupport.selectedChoicesToggled)
                        }
                        Section(header: Text("Langue(s)").bold()) {
                            LanguesView( selectedLanguages: selectedLanguages)
                        }
                        
                    }
                    Button(action: {
                        //              self.activated.toggle()
                        //   self.presenting.toggle()
                        //  self.resetButton()
                        self.activated = true
                        self.presenting = false
                        // construire la liste des resultats
                        self.resultsBuilder()
                        
                    }, label: {
                        Text("Appliquer").font(.headline).frame(width : 200, height: 50).background(Color.blue).foregroundColor(.white).cornerRadius(10)
                    })
                    
                    Spacer()
                }
                    //                .background(Color(red: 0.949, green: 0.949, blue: 0.968))
                    .navigationBarTitle("Filtres", displayMode : .inline)
                    .navigationBarItems(leading: Button(action: {
                        self.presenting.toggle()
                        self.resetButton()
                    }, label: {Image(systemName: "multiply").navBarItemLeadingStyle()}), trailing: Button(action: {
                        self.activated = false
                        self.resetButton()
                        // self.presenting.toggle()
                    }, label: {Text("Reinitialiser")
                        .navBarItemTrailingStyle()}))
            }
        }
        
    }
    // construction de la liste des résultats à afficher
    func resultsBuilder() {

        // on reset les résultats à chaque fois qu'on appuie sur appliquer pour appliquer les nouveaux filtres
        results.resetSources()
        // discipline large filter
        var filteredList : [Source] = allSources
        if indexDiscipline != .testAllDisciplines {
        filteredList = filteredList.filter({$0.disciplinesLarges.type == indexDiscipline})
        }
        // discipline spe filter (update later if you make an "all" category
        if indexDiscipline != .generaliste {
            print("discipline spé")
        }
        // niveau d'etude filter
        if indexAnneeEtude != .nonRenseigne {
            print("annee d'etude")
        }
        // domaine d'etude filter
        if agreedToTerms {
            print("domaine d'etude")
        }
        // cadre recherche filter
        if indexCadreRecherche != .nonRenseigne {
            print("cadre de recherche")
        }
        // format filter

        /* languages filter : we only have 2 languages, so apply filter if exactly 1 language is selected */
        var count : Int = 0
        for (_, value) in selectedLanguages.dicLangues {
            if value == true {
            count += 1
            }
        }
        if count == 1 {
            if selectedLanguages.dicLangues["EN"] == true {
                filteredList = filteredList.filter({$0.langues["EN"] == true})
            }
            else {
                filteredList = filteredList.filter({$0.langues["FR"] == true})
            }
        }

        /* add remaining sources in filteredList to results list */
        for source in filteredList {
            results.addSource(source) }
    }
}

struct ExtractionViewModal_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
    }
}
