//
//  TryingToImportJson.swift
//  MySciences
//
//  Created by Agnes Grele on 10/08/2020.
//  Copyright © 2020 Alice Grele. All rights reserved.
//

import SwiftUI

struct Country: Decodable {
    var country: String
    var code:String
}

struct CountryDetail: View {
    var country: Country
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 10) {
            Text(country.country)
                .font(.headline)
            Text(country.code)
                .font(.footnote)
        }
    }
}

struct TryingToImportJson: View {
    @State private var countryData = [Country]()
    var body: some View {
        NavigationView{List(countryData, id: \.code) { item in
            NavigationLink(destination: CountryDetail(country: item)) {
                
                HStack() {
                    Text(item.country)
                        .font(.headline)
                    Text(item.code)
                        .font(.footnote)
                }
                
            }.navigationBarTitle("Country List")
            
        }.onAppear(perform: loadData)}    }
}

extension TryingToImportJson
{
    func loadData() {
        
        guard let url = URL(string: "https://kaleidosblog.s3-eu-west-1.amazonaws.com/json/tutorial.json") else {
            return
        }
        
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let data = data {
                if let response_obj = try? JSONDecoder().decode([Country].self, from: data) {
                    
                    DispatchQueue.main.async {
                        self.countryData = response_obj
                    }
                }
            }
            
        }.resume()
    }
}



struct TryingToImportJson_Previews: PreviewProvider {
    static var previews: some View {
        TryingToImportJson()
    }
}
