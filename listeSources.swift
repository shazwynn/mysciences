//
//  listeSources.swift
//  MySciences
//
//  Created by Alice Grele on 08/07/2020.
//  Copyright © 2020 Alice Grele. All rights reserved.
//

import Foundation
/*
// https://api-eu-central-1.graphcms.com/v2/ckdkt3qe2hi5s01z8h3t5540m/master

struct Message : Codable {
    let content: String
}

final class MessageContext {
    func message() -> Message {
        Message(content: "Hello, world!")
    }
}

import Graphiti
struct MessageRoot {
    func message(context: MessageContext, arguments: NoArguments) -> Message {
        context.message()
    }
}
*/

/* THIS IS THE START OF HARD CODED SOURCES */

var listeSourcesDefaut : [Source] = [healthcareTriage, dansTonCorps, crashCourse, ourPlanet]
// update
var sourceSpotlight = healthcareTriage

// update
var listeScientistsDefaut : [Source] = [scientist2, scientist3, scientist4]
// update
var scientistSpotlight = scientist1

var scientist1 = Source(nom: "Francesca Alamena", auteur: "", image: "womaninlab", imageDetail: "womaninlabrectangle", imageSpotlight: "womaninlabrectangle", descriptionText: "Professeure de biochimie et biologie moléculaire, elle a découvert des protéines de la structure de l'ADN", detailText: "Professeure de biochimie et biologie moléculaire, elle a découvert des protéines impliquées dans la structure de l'ADN", disciplinesLarges: physique, anneeEtudeMin: "", anneeEtudeMax: "", format: "", langues: ["EN": true], cadresRecherche: [curiosite])

var scientist2 = Source(nom: "Christopher Langlet", auteur: "", image: "maskon", imageDetail: "maskonrectangle", imageSpotlight: "maskonrectangle", descriptionText: "Rédacteur scientifique, écrivain, naturaliste et créateur de nombreux documentaires animaliers.", detailText: "Rédacteur scientifique, écrivain naturaliste britannique. Il est à l'origine de nombreux documentaires animaliers.", disciplinesLarges: scienceVieTerre, anneeEtudeMin: "", anneeEtudeMax: "", format: "", langues: ["EN": true], cadresRecherche: [curiosite])

var scientist3 = Source(nom: "Mark Baldwin ", auteur: "", image: "whiteguy", imageDetail: "whiteguy", imageSpotlight: "whiteguy", descriptionText: "Ingénieur en analyse structurale, il travaille sur le système de lancement spatial de la NASA", detailText: "Ingénieur américain en analyse structurale, il travaille sur le système de lancement spatial de la NASA", disciplinesLarges: physique, anneeEtudeMin: "", anneeEtudeMax: "", format: "", langues: ["EN": true], cadresRecherche: [curiosite])

var scientist4 = Source(nom: "Akshara Adhya", auteur: "", image: "womaninyellow", imageDetail: "womaninyellowrectangle", imageSpotlight: "womaninyellowrectangle", descriptionText: "Astrophysicienne, elle a fait partie de l'équipe à l'origine de la découverte du premier pulsar", detailText: "Astrophysicienne, elle a fait partie de l'équipe à l'origine de la découverte du premier pulsar", disciplinesLarges: physique, anneeEtudeMin: "", anneeEtudeMax: "", format: "", langues: ["EN": true], cadresRecherche: [curiosite])

//class Sources: ObservableObject {
//    @Published var sourcesList : [Source]
//    @Published var scientistsList : [Source]
//    init() {
//        self.sourcesList = [healthcareTriage, dansTonCorps, crashCourse, ourPlanet, cafeyn, futura, epenser, tedx, teded, nationalgeographic, researcher, itunesu, scienceetavenir, unebrevehistoiredutemps, naissanceetdestindelunivers, scienceexplosive, pintofscience, researchgate, afterclasse]
//        self.scientistsList = [scientist1, scientist2, scientist3, scientist4]
//    }
//}

let allSources = [healthcareTriage, dansTonCorps, crashCourse, ourPlanet, cafeyn, futura, epenser, tedx, teded, nationalgeographic, researcher, itunesu, scienceetavenir, unebrevehistoiredutemps, naissanceetdestindelunivers, scienceexplosive, pintofscience, researchgate, afterclasse]

let allScientists = [scientist1, scientist2, scientist3, scientist4]

var healthcareTriage = Source(nom: "Healthcare Triage", auteur: "Dr. Aaron Carroll", image: "healthcaretriage", imageDetail: "healthcaretriagerectangle", imageSpotlight: "", descriptionText: "Vidéos présentées par le Dr. Aaron Carroll (USA) sur les thèmes de la recherche et politique de santé", detailText: "Présentée par le Dr. Aaron Carroll, professeur en pédiatrie et chargé du mentorat en recherche de la Indiana University School of Medicine (USA), cette chaîne présente de nombreuses courtes vidéos expliquant au grand public les problématiques autour de la politique de santé, la recherche médicale et de la Evidence Based Medicine (médecine basée sur les faits.", disciplinesLarges: medecine, anneeEtudeMin: seconde.title, anneeEtudeMax: bac5.title, format: video.title, support: Support.youtube, langues: ["EN": true], cadresRecherche: [curiosite], lien: "https://youtube.com/user/thehealthcaretriage")

var dansTonCorps = Source(nom: "DansTonCorps", auteur: "DansTonCorps", image: "danstoncorps", imageDetail : "danstoncorps", imageSpotlight: "", descriptionText: "Videos abordant des thèmes sur la santé, l'anatomie et le corps humain de manière humoristique", detailText: "Videos abordant des thèmes sur la santé, l'anatomie et le corps humain de manière humoristique", disciplinesLarges: medecine, disciplinesSpecifiques: ["anatomie"] ,anneeEtudeMin: seconde.title, anneeEtudeMax: bac5.title, format: video.title, support: Support.youtube, langues: ["FR" : true], cadresRecherche: [curiosite], isFavoris: true)

var crashCourse = Source(nom: "Crash Course", auteur: "Crash Course", image: "crashcourse", imageDetail: "crashcourse", imageSpotlight: "", descriptionText: "Séries très complètes de cours divertissants, en vidéos illustrées, dans de nombreuses disciplines", detailText: "Séries très complètes de cours divertissants, en vidéos illustrées, dans de nombreuses disciplines", disciplinesLarges: generaliste, anneeEtudeMin: sixieme.title, anneeEtudeMax: bac5.title, format: video.title, support: Support.youtube, langues: ["EN": true], cadresRecherche: [curiosite, revision])

var ourPlanet = Source(nom: "Our Planet", auteur: "David Attenborough", image: "ourplanet", imageDetail: "ourplanet", imageSpotlight: "", descriptionText: "Série documentaire de 8 épisodes illustrant la vie animale et le monde du vivant", detailText: "Série documentaire de 8 épisodes illustrant la vie animale et le monde du vivant", disciplinesLarges: scienceVieTerre, disciplinesSpecifiques: ["zoologie"] ,anneeEtudeMin: cp.title, anneeEtudeMax: bac5.title, format: video.title, support : Support.documentaire, langues: ["EN": true, "FR": true], cadresRecherche: [curiosite], isFavoris: true)

var cafeyn = Source(nom: "Cafeyn", auteur: "", image: "cafeyn", imageDetail: "cafeyn", imageSpotlight: "", descriptionText: "Cafeyn propose un abonnement à plus de 1600 journaux et magazines en illimité.", detailText: "Cafeyn propose un abonnement à plus de 1600 journaux et magazines en illimité", disciplinesLarges: generaliste, anneeEtudeMin: ce2.title, anneeEtudeMax: bac5.title, format: articlesVulgarisation.title, langues: ["FR" : true], cadresRecherche: [curiosite, recherche], isFavoris: true)

var futura = Source(nom: "Futura", auteur: "", image: "futura", imageDetail: "futura", imageSpotlight: "", descriptionText: "Futura est un portail d'information et de décryptage de l'actualité et du savoir scientifiques.", detailText: "Depuis sa création en 2001, Futura (anciennement Futura-Sciences) s’est imposé comme un grand média français du décryptage de l'actualité et du savoir scientifiques.", disciplinesLarges: generaliste, anneeEtudeMin: seconde.title, anneeEtudeMax: bac5.title, format: articlesVulgarisation.title, langues: ["FR" : true], cadresRecherche: [curiosite, recherche])

var epenser = Source(nom: "E-penser", auteur: "Bruce Benamran", image: "epenser", imageDetail: "epenser", imageSpotlight: "", descriptionText: "Emission de vulgarisation sur des sujets principalement scientifique, présentée par Bruce Benamran", detailText: "Emission de vulgarisation sur des sujets principalement scientifique, présentée par Bruce Benamran", disciplinesLarges: generaliste, anneeEtudeMin: seconde.title, anneeEtudeMax: bac5.title, format: video.title, langues: ["FR" : true], cadresRecherche: [curiosite,recherche])

var tedx = Source(nom: "TEDx", auteur: "", image: "tedx", imageDetail: "tedx", imageSpotlight: "", descriptionText: "TEDx est un programme qui permet aux particuliers et organisations d'organiser des conférences TED pour propager le savoir", detailText: "TEDx est un programme qui permet aux écoles, aux entreprises, aux bibliothèques ou aux groupes d'amis de profiter d'une expérience semblable à celle de TED par le biais d'événements qu'ils organisent eux-mêmes", disciplinesLarges: generaliste, anneeEtudeMin: seconde.title, anneeEtudeMax: bac5.title, format: video.title, langues: ["FR" : true, "EN": true], cadresRecherche: [curiosite, recherche])

var teded = Source(nom: "TEDed", auteur: "", image: "teded", imageDetail: "teded", imageSpotlight: "", descriptionText: "TedEd est un outil en ligne qui permet de créer des leçons vidéo interactives et de les proposer à vos étudiants.", detailText: "TedEd est un outil en ligne qui permet de créer des leçons vidéo interactives et de les proposer à vos étudiants.", disciplinesLarges: generaliste, anneeEtudeMin:ce2.title, anneeEtudeMax: bac5.title, format: video.title, langues: ["FR": true, "EN": true], cadresRecherche: [curiosite, recherche])

var nationalgeographic = Source(nom: "National Geographic", auteur: "", image: "nationalgeographic", imageDetail: "nationalgeographic", imageSpotlight: "", descriptionText: "National Geographic, média de référence dans la science et l'exploration.", detailText: "National Geographic, média de référence dans la science et l'exploration.", disciplinesLarges: generaliste, anneeEtudeMin: quatrieme.title, anneeEtudeMax: bac5.title, format: articlesVulgarisation.title, langues: ["FR" : true], cadresRecherche: [curiosite])

var researcher = Source(nom: "Researcher", auteur: "", image: "researcher", imageDetail: "researcher", imageSpotlight: "", descriptionText: "Researcher est une application gratuite pour les universitaires, permettant de trouver et lire facilement de nouvelles études.", detailText: "Researcher est une application gratuite pour les universitaires, permettant de trouver et lire facilement de nouvelles études.", disciplinesLarges: generaliste, anneeEtudeMin: bac2.title, anneeEtudeMax: bac5.title, format: publicationScientifique.title, langues: ["FR" : true], cadresRecherche: [recherche])

var itunesu = Source(nom: "iTunes U", auteur: "", image: "itunesu", imageDetail: "itunesurectangle", imageSpotlight: "", descriptionText: "Plateforme de contenus éducatifs proposés par des Universités : cours en ligne, conférences, videos, podcasts...", detailText: "iTunes U est une plateforme de contenus éducatifs proposant des cours en ligne, conférences, videos, podcasts, ", disciplinesLarges: generaliste, anneeEtudeMin: quatrieme.title, anneeEtudeMax: bac5.title, format: cours.title, langues: ["FR" : true, "EN": true], cadresRecherche: [recherche])

var scienceetavenir = Source(nom: "Science et Avenir", auteur: "", image: "scienceetavenir", imageDetail: "scienceetavenir", imageSpotlight: "", descriptionText: "Magazine de vulgarisation scientifique : sciences naturelles, humaines, environnement, archéologie, exploration spatiale...", detailText: "Toute l'actualité scientifique : sciences naturelles, scienceshumaines, environnement, archéologie, exploration spatiale...", disciplinesLarges: generaliste, anneeEtudeMin: seconde.title, anneeEtudeMax: bac5.title, format: articlesVulgarisation.title, langues: ["FR" : true], cadresRecherche: [curiosite])

var unebrevehistoiredutemps = Source(nom: "Une brève histoire du temps", auteur: "Stephen Hawking", image: "unebrevehistoiredutemps", imageDetail: "unebrevehistoiredutemps", imageSpotlight: "", descriptionText: "ouvrage de vulgarisation scientifique qui traite de cosmologie, la science des lois qui gouvernent l’univers", detailText: "ouvrage de vulgarisation scientifique qui traite de cosmologie, la science des lois qui gouvernent l’univers", disciplinesLarges: physique, anneeEtudeMin: bac2.title, anneeEtudeMax: bac5.title, format: ouvrage.title, langues: ["FR" : true], cadresRecherche: [curiosite])

var naissanceetdestindelunivers = Source(nom: "Naissance et destin de l'univers", auteur:"Paul Parsons", image: "naissanceetdestindelunivers", imageDetail: "naissanceetdestindelunivers", imageSpotlight: "", descriptionText: "L'histoire de notre univers telle qu'on la comprend aujourd'hui : du néant, à la naissance de l'univers, et jusqu'à son destin final.", detailText: "Naissance et destin de l'univers, c'est toute l'histoire de notre univers telle qu'on la comprend aujourd'hui : du néant, à la naissance de l'univers, et jusqu'à son destin final. ... ", disciplinesLarges: scienceVieTerre, anneeEtudeMin: seconde.title, anneeEtudeMax: bac5.title, format: audio.title, langues: ["FR" : true], cadresRecherche: [curiosite, recherche])

var scienceexplosive = Source(nom: "Science explosive", auteur: "", image: "scienceexplosive", imageDetail: "scienceexplosive", imageSpotlight: "", descriptionText: "Expériences explosives, amusantes et sans danger à partir de 8 ans : la bombe, le geyser, la fusée et plein d'autres.", detailText: "Science explosive permet de réaliser des expériences explosives et amusantes sans danger. Vous comprendrez comment fonctionne une fusée, ferez des bombes crépitantes et vous amuserez avec le sac explosif très surprenant pour tous vos amis. ", disciplinesLarges: physique, anneeEtudeMin: ce2.title, anneeEtudeMax: bac5.title, format: jeu.title, langues: ["FR" : true], cadresRecherche: [curiosite])

var pintofscience = Source(nom: "Pint of Science", auteur: "", image: "pintofscience", imageDetail: "pintofscience" , imageSpotlight: "", descriptionText: "Le Festival Pint of Science invite de brillants scientifiques dans votre bar préféré pour discuter avec vous de leurs dernières recherches et découvertes.", detailText: "Le Festival Pint of Science invite de brillants scientifiques dans votre bar préféré pour discuter avec vous de leurs dernières recherches et découvertes. C’est pour vous l’occasion parfaite de rencontrer les acteurs de la science de demain en chair et en os. ", disciplinesLarges: generaliste, anneeEtudeMin: terminale.title, anneeEtudeMax: bac5.title, format: evenement.title, langues: ["EN": true, "FR" : true], cadresRecherche: [curiosite, recherche])

var researchgate = Source(nom: "ResearchGate", auteur: "", image: "researchgate", imageDetail: "researchgate" , imageSpotlight: "", descriptionText: "Moteur de recherche bibliographique et réseau social pour chercheurs et scientifiques de toutes disciplines", detailText: "Moteur de recherche bibliographique et réseau social pour chercheurs et scientifiques de toutes disciplines", disciplinesLarges: generaliste, anneeEtudeMin: bac2.title, anneeEtudeMax: bac5.title, format: publicationScientifique.title, langues: ["EN": true, "FR" : true], cadresRecherche: [recherche], isFavoris: true)

var afterclasse = Source(nom: "afterclasse", auteur: "", image: "afterclasse", imageDetail: "afterclasse", imageSpotlight: "", descriptionText: "Site de révisions ludique qui s'adapte à ton niveau ! Fiches et exercices pour toutes les matières, annales bac & brevet.", detailText: "Le site de révisions ludique qui s'adapte à ton niveau ! Fiches et exercices pour toutes les matières, annales bac & brevet. Conforme au programme officiel.\nSur le chemin de l'école, dans ta chambre ou 5 minutes avant un contrôle : emporte tes fiches de révisions dans ta poche avec l'application mobile Afterclasse.\nDes milliers d'exercices interactifs qui s'adaptent à ton niveau. Ton objectif est d'avoir le bac à la fin de l'année ? De décrocher la mention très bien ? Afterclasse te fait réviser à ton rythme jusqu’au jour J.", disciplinesLarges: generaliste, anneeEtudeMin: sixieme.title, anneeEtudeMax: terminale.title, format: cours.title, support: Support.application, langues: ["FR" : true], cadresRecherche: [revision], lien: "https://www.afterclass.fr")
