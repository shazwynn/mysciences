//
//  MySourcesDetailView.swift
//  MySciences
//
//  Created by Alice Grele on 08/07/2020.
//  Copyright © 2020 Alice Grele. All rights reserved.
//

import SwiftUI

struct bigBookmarkToggleStyle: ToggleStyle {
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
            Image(systemName: configuration.isOn ? "bookmark.fill" : "bookmark")
                .resizable()
                .foregroundColor(.black)
                .frame(width: 20, height: 30)
                .onTapGesture {
                    configuration.isOn.toggle()
            }
        }
    }
}
struct MySourcesDetailView: View {
    @EnvironmentObject var favorites: Favorites
    var source : Source
    var body: some View {
        VStack {
            Image(source.imageSpotlight.isEmpty ? source.imageDetail : source.imageSpotlight)
                .resizable()
                .scaledToFit()
                .frame(width: self.largeurRectangle(), height: self.hauteurRectangle())
                .cornerRadius(20)
                .padding(.top)
            Text(source.detailText)
                .font(.subheadline)
                .padding()
            Text(String(source.lien ?? ""))
                .font(.footnote)
                .underline()
            Spacer()
                .navigationBarTitle(Text(source.nom), displayMode: .inline)
                .navigationBarItems(trailing: Button(action: {
                    if self.favorites.containsSource(self.source) {
                        self.favorites.removeSource(self.source)
                    } else {
                        self.favorites.addSource(self.source)
                    }
                }, label: {
                    HStack() {
                        Image(systemName: favorites.containsSource(source) ? "bookmark.fill" : "bookmark")
                        .resizable()
                        .foregroundColor(.black)
                        .frame(width: 20, height: 30)
                    }
                }))

        }
    }    // 5 is the number of spacings in the HStack row
    // 12 is the spacing in the HStack between buttons
    func hauteurRectangle() -> CGFloat {
        return(UIScreen.main.bounds.height - 1 * 27 ) / 4
    }
    func largeurRectangle() -> CGFloat {
        return(UIScreen.main.bounds.width - 2 * 13 ) / 1
    }
}

struct MySourcesDetailView_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
    }
}
