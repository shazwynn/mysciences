//
//  ToDo.swift
//  MySciences
//
//  Created by Agnes Grele on 06/08/2020.
//  Copyright © 2020 Alice Grele. All rights reserved.
//

/*

TO DO list MySciences

---- Bigger things ----
 
1. Store data online and import
2. Apply filters with filtering page :
    - discipline large OK
    - langues OK
3. Favoris userdefaults
 
---- Smaller things ----
 
 - create scientist type, fix default/result page for MyScientists, fix big spotlight scrolling on MyScientists
- update details view : working links, related content
- Make more data
 
----- Accessibility -----
- Resize for different screen sizes
- Accessibility

- Sondage
- All code in English
- Check if names conform to guidelines : views always take capital letter ?

IN PROGRESS

- Clean up code
- Is navigation broken to details view in some places?


DONE
- Apply filters in favoris
- Navigation links in favorites (but broke bookmark buttons)
- Debug favoris : if I add Francesca to favorites, then go to favorites and open her details page, the icon in the details page isn’t filled
 - Make big spotlight and small spotlight into one list view
-  Fix awful default/results pages implementation ?

*/
