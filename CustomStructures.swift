//
//  CustomStructures.swift
//  MySciences
//
//  Created by Alice Grele on 08/07/2020.
//  Copyright © 2020 Alice Grele. All rights reserved.
//

import Foundation

///


/*Class pour objet contenant les choix de filtres
class Filtres : ObservableObject {
    
    // Si au moins une disci large est selectionnee
    var disciplinesLargesisSelected = false
    
    // Selection d'une discipline large
    var disciplinesLargesIsBiologie = false
    var disciplinesLargesIsChimie = false
    var disciplinesLargesIsScienceVieTerre = false
    var disciplinesLargesIsMedecine = false
    var disciplinesLargesIsInformatique = false
    var disciplinesLargesIsGeneraliste = false
    
    // Selection des disciplines specifiques
    var disciplinesSpecifiquesisSelected = false
    var disciplinesSpecifiques : [String]? = nil
    
    // Niveau d'étude
    //  var niveauEtude : NiveauEtude = NiveauEtude.lycee
    var anneeEtude : String = ""
    
    // format, langues et cadre
    var format : [String] = []
    var langues : [LangueType] = [.francais, .anglais]
    var cadreRecherche : String = ""
    
}
*/
//Page MySource main

//Objets necessaires : Sources

//Structure d'une source
struct Source : Identifiable{
    //affichage
    let id = UUID()
    var nom : String
    var auteur : String
    var image : String
    var imageDetail : String
    var imageSpotlight : String
    var descriptionText : String
    var detailText : String
    
    //filtre
    var disciplinesLarges : Discipline
    var disciplinesSpecifiques : [String]?
    //  var niveauxEtude : [NiveauEtude]
    var anneeEtudeMin : String
    var anneeEtudeMax : String
    var format : String
    var support : Support?
    var langues : [String: Bool] = [:]
    var cadresRecherche : [CadreRecherche]
    
    var isFavoris : Bool = false
    
    var lien : String?
    
    var sourcesAnnexe : [Source]?
    func sourceText(source: Source) -> String {
        var text = source.nom
        if let support = source.support?.rawValue.capitalized {
            text += " - "
            text += support
        }
        else if !(format.isEmpty){
            text += " - "
            text += source.format.capitalized
        }
        return text
    }
    //Text à voir pour les sources annexe
    
    //Lien vers le detail…?
}

///

//determiner la discipline

enum DisciplineType : CaseIterable {
    case generaliste, chimie, physique, medecine, scienceVieTerre, informatique, testAllDisciplines
    var showingSpe : Bool {
        switch self {
        case .chimie, .physique, .informatique, .medecine, .scienceVieTerre :
            return true
        case .generaliste, .testAllDisciplines:
            return false
        }
    }
    
    var values : [DisciplineSpe] {
        switch self {
        case .chimie :
            return [c1, c2, c3, c4, c5]
        case .physique :
            return [p1, p2, p3, p4, p5]
        case .informatique :
            return [i1,i2,i3,i4,i5]
        case .medecine :
            return [anatomie,m2,m3,m4,m5]
        case .scienceVieTerre :
            return [zoologie,svt2,svt3,svt4,svt5]
        case .generaliste, .testAllDisciplines :
            return []
        }
    }
 
}

//strcut Discipline
struct Discipline : Identifiable {
    var id = UUID()
    
    let title : String
    let type : DisciplineType
}

// instances de Discipline
var generaliste = Discipline(title: "Généraliste", type: .generaliste)
var chimie = Discipline(title: "Chimie", type: .chimie)
var physique = Discipline(title: "Physique", type: .physique)
var informatique = Discipline(title: "Sciences du numérique", type: .informatique)
var medecine = Discipline(title: "Médecine", type: .medecine)
var scienceVieTerre = Discipline(title: "Sciences et vie de la Terre", type: .scienceVieTerre)
var testAllDisciplines = Discipline(title: "All Disciplines (test)", type: .testAllDisciplines)


//tableau all discipline
let allDiscipline = [physique, chimie, scienceVieTerre, medecine, informatique, generaliste, testAllDisciplines]



/////Choix langues
/*
enum LangueType : CaseIterable {
    case francais, anglais
}
*/

//choix support
enum Support : String {
    case application = "Application", documentaire = "Documentaire", youtube = "YouTube", moteurDeRecherche = "Moteur de recherche"
    static var all :  [Support] = [.application, .documentaire, .youtube, .moteurDeRecherche]
}

////choix format
enum FormatType : CaseIterable {
    case arVr, articleVulgarisation, audio , cours , evenement, jeu , ouvrage, publicationScientifique , texteContenuInstructif, video
}
/*
struct Format : Identifiable
{
    var id = UUID()
    let title : String
    let type : FormatType
}

var arVr = Format(title: "realite virtuelle et augmentée", type : .arVr)
var articleVulgarisation = Format(title : "publication de vulgarisation", type: .articleVulgarisation)
var audio = Format(title: "audio", type : .audio)
var cours = Format(title: "cours", type: .cours)
var evenement = Format(title: "évènement", type: .evenement)
var jeu = Format(title: "jeu", type: .jeu)
var ouvrage = Format(title: "ouvrage", type: .ouvrage)
var publicationScientifique = Format(title: "publication scientifique", type: .publicationScientifique)
var texteContenuInstructif = Format(title: "article éducatif", type: .texteContenuInstructif)
var video = Format(title: "video", type: .video)

 */
//Cadre de la recherche
enum CadreRechercheType : CaseIterable {
    case nonRenseigne, curiosite, revision, recherche
//    static var all : [CadreRecherche] = [.curiosite, .expose, .rechercheUniversitaire, .revision]
}
struct CadreRecherche : Identifiable {
    var id = UUID()
    
    let title : String
    let type : CadreRechercheType
}
var curiosite = CadreRecherche(title: "Par curiosité", type: .curiosite)
var revision = CadreRecherche(title: "Aides aux révisions", type: .revision)
var recherche = CadreRecherche(title: "Recherches (Devoirs, Exposés, Mémoire…)", type: .recherche)

let allCadres = [curiosite, revision, recherche]

//determiner le niveau d'étude

enum AnneeEtude : CaseIterable {
    case nonRenseigne
    case cp, ce1, ce2, cm1, cm2
    case sixieme, cinquieme, quatrieme, troisieme
    case seconde, premiere, terminale
    case bac2, bac3, bac5
    
    var disableling : Bool {
        switch self {
        case .nonRenseigne :
            return false
        case .cp, .ce1, .ce2, .cm1, .cm2 :
            return false
        case .sixieme, .cinquieme, .quatrieme, .troisieme :
            return false
        case .seconde, .premiere, .terminale :
            return true
        case .bac2, .bac3, .bac5 :
            return true
        }
    }
}

struct Annee : Identifiable {
    var id = UUID()
    
    let title : String
    let type : AnneeEtude
}

// instances annee etudes
var nonRenseigne = Annee(title: "Non renseigné", type: .nonRenseigne)

var cp = Annee(title: "CP", type: .cp)
var ce1 = Annee(title: "CE1", type: .ce1)
var ce2 = Annee(title: "CE2", type: .ce2)
var cm1 = Annee(title: "CM1", type: .cm1)
var cm2 = Annee(title: "CM2", type: .cm2)

var sixieme = Annee(title: "6ème", type: .sixieme)
var cinquieme = Annee(title: "5ème", type: .cinquieme)
var quatrieme = Annee(title: "4ème", type: .quatrieme)
var troisieme = Annee(title: "3ème", type: .troisieme)

var seconde = Annee(title: "2nde", type: .seconde)
var premiere = Annee(title: "1ère", type: .premiere)
var terminale = Annee(title: "Terminale", type: .terminale)

var bac2 = Annee(title:  "Bac+2 (DUT/BTS/Autre)", type: .bac2)
var bac3 = Annee(title: "Bac+3/4 (Licence/Maîtrise/Autre)", type: .bac3)
var bac5 = Annee(title: "Bac+5 et plus (Master/Ingénieur/Doctorat)", type: .bac5)

let allAnneePrimaire = [nonRenseigne, cp, ce1, ce2, cm1, cm2]
let allAnneeCollege = [sixieme, cinquieme, quatrieme, troisieme]
let allAnneeLycee = [seconde, premiere, terminale]
let allAnneeSup = [bac2, bac3, bac5]

enum DomaineEtudeType : CaseIterable {
    case nonRenseigne, chimie, physique, medecine, sciencesVieTerre, informatique, nonScientifique
    
//    static var all : [DomaineEtude] = [.defaultVal, .nonScientifique, .chimie, .physique, .sciencesVieTerre, .informatique]
}

struct DomaineEtude : Identifiable {
    var id = UUID()
    
    let title : String
    let type : DomaineEtudeType
}
//Instances de domaine etudes

var noneRenseigne = DomaineEtude(title: "Non renseigné", type: .nonRenseigne)
var chimies = DomaineEtude(title: "Chimie", type: .chimie)
var physiques = DomaineEtude(title: "Physique", type: .physique)
var medecines = DomaineEtude(title: "Médecine", type: .medecine)
var svt = DomaineEtude(title: "Sciences et vie de la  Terre", type: .sciencesVieTerre)
var informatiques = DomaineEtude(title: "Sciences du numérique", type: .informatique)
var nonScientifiques = DomaineEtude(title: "Autre non-scientifique", type: .nonScientifique)

let allDomaineEtude = [noneRenseigne, chimies, physiques, medecines, svt, informatiques, nonScientifiques]
//

///

//moule MyScientist

struct Scientist {
    
    var nom : String
    var pays : String
    var disciplinesLargesScientist : [Discipline]
    var dateVieMort : String
    
    var image : String
    var imageDetail : String?
    var descriptionText : String
    var detailText : String
    
    var isFavoris : Bool
    var publications : [Source]
    var recommandations : [Source]
    
}
///
//instances disciplines spe
//var c1 = DisciplineSpe(title: "C1", type: .chimie)
//var c2 = DisciplineSpe(title: "C2", type: .chimie)
//var c3 = DisciplineSpe(title: "C3", type: .chimie)
//var c4 = DisciplineSpe(title: "C4", type: .chimie)
//var c5 = DisciplineSpe(title: "C5", type: .chimie)
//
//var p1 = DisciplineSpe(title: "P1", type: .physique)
//var p2 = DisciplineSpe(title: "P2", type: .physique)
//var p3 = DisciplineSpe(title: "P3", type: .physique)
//var p4 = DisciplineSpe(title: "P4", type: .physique)
//var p5 = DisciplineSpe(title: "P5", type: .physique)
//
//var i1 = DisciplineSpe(title: "I1", type: .informatique)
//var i2 = DisciplineSpe(title: "I2", type: .informatique)
//var i3 = DisciplineSpe(title: "I3", type: .informatique)
//var i4 = DisciplineSpe(title: "I4", type: .informatique)
//var i5 = DisciplineSpe(title: "I5", type: .informatique)
//
//var anatomie = DisciplineSpe(title: "Anatomie", type: .medecine)
//var m2 = DisciplineSpe(title: "M2", type: .medecine)
//var m3 = DisciplineSpe(title: "M3", type: .medecine)
//var m4 = DisciplineSpe(title: "M4", type: .medecine)
//var m5 = DisciplineSpe(title: "M5", type: .medecine)
//
//var zoologie = DisciplineSpe(title: "Zoologie", type: .scienceVieTerre)
//var svt2 = DisciplineSpe(title: "SVT2", type: .scienceVieTerre)
//var svt3 = DisciplineSpe(title: "SVT3", type: .scienceVieTerre)
//var svt4 = DisciplineSpe(title: "SVT4", type: .scienceVieTerre)
//var svt5 = DisciplineSpe(title: "SVT5", type: .scienceVieTerre)
